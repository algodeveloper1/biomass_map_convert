# -*- coding: utf-8 -*-
from osgeo import gdal
from gdalconst import GA_ReadOnly
import numpy as np
import matplotlib.pyplot as plt
import os

def conversion(inputFilename, outputFilename, outputmap):

    # Open input image in ground range geometry:
    input_image_driver = gdal.Open(inputFilename, GA_ReadOnly)
    input_image = input_image_driver.ReadAsArray()
    ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####
  
    with np.errstate(invalid='ignore'): # To avoid warnings when computations with NaN
        # Conversion in dB:
        output_image = 10 * np.log10(input_image)
        
        # Set a minimum threshold of -35:
        output_image[output_image < -35] = -35
    
    # Elegant way to add constant value on the whole scene
    output_image += 50
    
    print(np.nanmin(output_image))
    fig, ax = plt.subplots()
    img1 = ax.imshow(output_image,cmap='Greens',vmin=15,vmax=50)
    fig.colorbar(img1, ax=ax)
    plt.savefig(outputmap,dpi=1000)
    plt.close(fig)
    

    ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####
    # Save output image in ground range geometry:
    outdriver = gdal.GetDriverByName('GTiff')
    output_image_driver = outdriver.Create(outputFilename, input_image_driver.RasterXSize, input_image_driver.RasterYSize, 1, gdal.GDT_Float32)
    output_image_driver.SetGeoTransform(input_image_driver.GetGeoTransform()) # To keep the georeferencing
    output_image_driver.SetProjection(input_image_driver.GetProjection()) # To keep the georeferencing
    output_image_driver.GetRasterBand(1).WriteArray(output_image)

    # Close data sets:
    input_image_driver = None
    output_image_driver = None

if __name__ == '__main__':
 

    from properties.p import Property
    import quicklook_raster
    if os.path.isfile('/projects/biomass_map_convert/conf/configuration.properties'):
            
        configfile='/projects/biomass_map_convert/conf/configuration.properties'
        print('The processing is launched using ', configfile)
        prop=Property()
        prop_dict=prop.load_property_files(configfile)
    
        conversion(prop_dict['inputfile'], prop_dict['outputfile'],prop_dict['outputmap'])
        

